const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");
const app = express();

const port = process.env.PORT || 5000;

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(express.static(__dirname + "/public"));
app.use("/uploads", express.static("uploads"));

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({
  storage: storage,
});

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

app.post("/uploadfile", upload.single("myFile"), (req, res, next) => {
  const file = req.file;

  if (!file) {
    const error = new Error("please upload file");
    console.log(file);
    error.httpsourceCode = 400;
    return next(err);
  }
  res.send(file);
});

app.post("/uploadmultiple", upload.array("myFiles", 12), (req, res, next) => {
  const files = req.files;

  if (!files) {
    const error = new Error("please upload file");
    console.log(files);
    error.httpsourceCode = 400;
    return next(err);
  }
  res.send(files);
});
app.post("/uploadphoto", upload.single("myimg"), (req, res, next) => {
  console.log(JSON.stringify(req.file));
  var response = "Files uploaded successfully.<br>";
  response += `<img src="${req.file.path}" /><br>`;
  return res.send(response);
});

app.post("/uploadphotom", upload.array("myImg", 12), (req, res, next) => {
  console.log(JSON.stringify(req.file));
  var responde = "Files uploaded successfully.<br>";

  for (var i = 0; i < req.files.length; i++) {
    responde += `<img src="${req.files[i].path}" /><br>`;
  }
  return res.send(responde);
});

app.listen(port, () => {
  console.log(`server is listening ${port}`);
});
